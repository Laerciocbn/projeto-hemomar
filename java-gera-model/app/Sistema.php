<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sistema extends Model
{
	protected $connection = 'mysql';
	protected $table = 'TBG_sistema';
	protected $primaryKey = 'SIST_id';
	public $timestamps = false;

	//campos
	public static $id = 'SIST_id';
	public static $descricao = 'SIST_descricao';
	public static $codigo = 'SIST_codigo';

	//Relacionamentos
	public function Classes(){return $this->hasMany('App\Classe',Classe::$fk_sistema);}
	public function Logs(){return $this->hasMany('App\Log',Log::$fk_sistema);}
	public function Rotases(){return $this->hasMany('App\Rotas',Rotas::$fk_sistema);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Sistema::$id];}
	public function getDescricao(){return $this->attributes[Sistema::$descricao];}
	public function getCodigo(){return $this->attributes[Sistema::$codigo];}

	//Set's
	public function setId($valor){$this->attributes[Sistema::$id] = $valor;}
	public function setDescricao($valor){$this->attributes[Sistema::$descricao] = $valor;}
	public function setCodigo($valor){$this->attributes[Sistema::$codigo] = $valor;}

}