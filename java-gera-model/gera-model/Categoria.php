<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
	protected $connection = 'mysql';
	protected $table = 'ENGE_categoria';
	protected $primaryKey = 'CATEG_id';

	//campos
	public static $id = 'CATEG_id';
	public static $descricao = 'CATEG_descricao';

	//Get's
	public function getId(){return $this->attributes[Categoria::$id];}
	public function getDescricao(){return $this->attributes[Categoria::$descricao];}

	//Set's
	public function setId($valor){$this->attributes[Categoria::$id] = $valor;}
	public function setDescricao($valor){$this->attributes[Categoria::$descricao] = $valor;}

}