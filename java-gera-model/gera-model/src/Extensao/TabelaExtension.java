/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extensao;

import Entidade.Util;

/**
 *
 * @author ronney
 */
public class TabelaExtension
{
    public static String nomeArquivo(String tabela)
    {
        String[] split = tabela.split("_");
        String nome = "";
        
        for(int x = 1; x < split.length; x++)
        {
            nome += Util.primeiraMaiuscula(split[x]);
        }
        
        return nome;
    }
}
