<div class="modal fade" id="dlgDelete" role="dialog">
    <div class="modal-dialog">
        <form action="{{route($rota)}}" method="POST">
            @csrf

            <input type="hidden" id="id_delete_modal" name="id"/>

            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="titulo_delete_modal" class="modal-title">{{$titulo}}</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <h3>{{$mensagem_delete}} <b id="item_delete"></b>?</h3>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="container-fluid">
                        <button type="submit" class="btn btn-primary">Confirmar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>                                
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>