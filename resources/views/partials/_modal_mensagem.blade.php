<div id="dlgMensagem" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <!-- Modal content-->
        <div class="modal-content" >
            <div id="header_modal_mensagem" class="modal-header">
                <i id="icon_modal_mensagem" style="font-size:30px;padding-right:10px;"></i>
                <h4 id="titulo_modal_mensagem" class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid" style="text-overflow: ellipsis;word-wrap: break-word;">

                        <div id="msg_modal_mensagem"></div>

                    <hr>
                    <div class="float-right">
                        <br/>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
