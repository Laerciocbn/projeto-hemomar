<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ URL::asset(env('APP_FAVICON'))}}">

    <title>{{env('APP_NAME')}}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap_v4/bootstrap.min.css') }}" >
    <link rel="stylesheet" href="{{ URL::asset('fontawesome/css/fontawesome.min.css')}}" >
    <link rel="stylesheet" href="{{ URL::asset('fontawesome/css/solid.css')}}" >
    <link rel="stylesheet" href="{{ URL::asset('css/views/layout.css') }}" >
    <link rel="stylesheet" href="{{ URL::asset('css/views/master.css') }}" >

    @yield('css')

</head>
<body>

    <!-- Loading -->
    <div class="se-pre-con text-center">
        <i class="fa fa-spinner fa-spin fa-4x fa-fw text-success centralizar"></i>
    </div>

    <!-- Sidebar  -->
    <div id="containerPrincipal" class="wrapper">
      
        @auth
            @include('partials._menu')
        @endauth

        <!-- Page Content  -->
        <main id="content" class="p-0">

            
            @auth
                @include('partials._barra_superior')
            @endauth
            

            <div id="paginaConteudo">
                @yield('pagina')
            </div>


        </main>
    </div>
	
	<!-- mensagem -->
    @include('partials._modal_mensagem')

    <!-- Scripts -->
    <script src="{{ URL::asset('js/jquery_v3.3.1/jquery-3.3.1.min.js') }}" ></script>
    <script src="{{{ URL::asset('js/popper/popper.min.js') }}}"></script>
    <script src="{{ URL::asset('js/bootstrap_v4/bootstrap.min.js') }}" ></script>
    <script src="{{ URL::asset('js/JqueryMaxLength/jquery.maxlength.js') }}" ></script>
    <script src="{{ URL::asset('js/jquery_mask/jquery.mask.min.js') }}" ></script>
    <script src="{{ URL::asset('js/views/layout.js') }}" ></script>
    
	
	 <!-- Mensagens do controller -->
    <script type="text/javascript">

        @if (count($errors) > 0)

            var message = "";

            @foreach($errors->all() as $error)
                message += "{{ $error }}\n";
            @endforeach

            MensagemBox('erro','Erro',message);

        @endif

        @if(session('sucesso'))

            MensagemBox ('sucesso','Sucesso',"{{ session('sucesso') }}");

        @elseif(session('alerta'))

            MensagemBox ('alerta','Atenção',"{{ session('alerta') }}");

        @elseif(session('alerta-vermelho'))

            MensagemBox ('alerta-vermelho','Atenção',"{{ session('alerta-vermelho') }}");

        @elseif(session('info'))

            MensagemBox ('info','Informação',"{{ session('info') }}");

        @endif


    </script>

    @yield('scripts')

</body>
</html>
