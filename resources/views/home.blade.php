@extends('layouts.app')

@section('titulo')
    <i class="fas fa-chart-line"></i> Dashboard
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/charts/highcharts.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts//highcharts-more.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts/modules/solid-gauge.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts/modules/exporting.js') }}}"></script>
    <script src="{{{ URL::asset('js/charts/modules/export-data.js') }}}"></script>

    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>

    <script src="{{{ URL::asset('js/views/home.js') }}}"></script>
@endsection

@section('pagina')

    <div class="container-fluid">
		<div class="panel panel-primary ">

            <div class="row">

                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #339966;">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <i class="fa fa-tags fa-4x text-white mt-3"></i>
                                <h0 class="float-right text-white">{{$pendentes}}</h0>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h5 class="text-white font-weight-bold">Total Pendentes</h5>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #39ac73;">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <i class="fa fa-users-cog fa-4x text-white mt-3"></i>
                                <h0 class="float-right text-white">{{$andamento}}</h0>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h5 class="text-white font-weight-bold">Total em andamento</h5>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #40bf80;">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <i class="fa fa-check-circle fa-4x text-white mt-3"></i>
                                <h0 class="float-right text-white">{{$concluidos}}</h0>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h5 class="text-white font-weight-bold">Concluídos</h5>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 mb-3">
                    <div class="panel panel-primary quadro-dash pl-2 pr-2" style="background-color: #53c68c;">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <i class="fa fa-stopwatch fa-4x text-white mt-3"></i>
                                <h0 class="float-right text-white">{{$tma}}</h0>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h5 class="text-white font-weight-bold">Tempo médio</h5>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <br/>
            <div class="row">

                <div class="col-md-8" >
                    <div class="panel panel-primary quadro-chart" style="background-color: #fff;">
                        <div class="panel-heading">
                            <div class="container">
                                <br>
                                <div class="col-lg-12">
                                    <h5 style="color: #999999;">Chamados em 2018</h5>
                                    <div class="card bar-chart-example">
                                        <div class="card-body">
                                            <div id="chartContainer" style="height: 400px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4" >
                    <div class="panel panel-primary quadro-chart" style="background-color: #fff;">
                        <div class="panel-heading">
                            <div class="container">
                                <br>
                                <div class="col-lg-12">
                                    <h5 style="color: #999999;">Posição dos Chamados</h5>
                                    <div class="card bar-chart-example">
                                        <div class="card-body">
                                            <div id="chartContainer2" style="height: 400px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

	    </div>

    </div>


@endsection
