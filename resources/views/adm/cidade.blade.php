@extends('layouts.app')

@section('titulo')
    <i class="fas fa-city"></i> Cadastro de Cidades
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/adm/cidade.js') }}}"></script>
@endsection

@section('pagina')


    <div class="container-fluid">
        <div class="row" >
            <div class="col panel panel-primary table-responsive">

                <table id="tbCidades" class="table table-hover text-left">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Estado</th>
                            <th>UF</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($cidades as $cidade)
                            <tr>
                                <td id="{{$cidade->getId()}}_nome">{{$cidade->getNome()}}</td>
                                <td id="{{$cidade->getId()}}_estado" data="{{$cidade->Estado->getId()}}">{{$cidade->Estado->getNome()}}</td>
                                <td id="{{$cidade->getId()}}_uf">{{$cidade->Estado->getSigla()}}</td>
                                <td>
                                    <span name="editBtn" class="fas fa-edit" data="{{$cidade->getId()}}" style="cursor: pointer;color: #009933;" title="Editar"></span>
                                    <span name="delBtn" class="fas fa-trash-alt" data="{{$cidade->getId()}}" style="cursor: pointer;color: #ff0000;" title="Excluir"></span>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Atualizar Cidade',
		'icone'=>'fa-city',
        'rota' => 'adm.cidade.salvar',
        'campos'=> 
        [
            [
                'id' =>'txtNome',
                'label'=>'Nome',
                'nome'=>'nome',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'cbEstado',
                'label'=>'Estado',
                'nome'=>'uf',
                'tipo'=>'combo',
                'opcoes'=> $estados,
                'default'=>'Selecione',
                'required'=> true, 
                'autofocus'=> false,
                'disabled'=>false
            ],
        ]
    ])

@endsection