@extends('layouts.app')

@section('titulo')
    <i class="fas fa-user-tag"></i> Classes
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/adm/classes.js') }}}"></script>
@endsection

@section('pagina')

    <div class="container-fluid">
        <div class="row" >
            <div class="col text-left">
                <button id="btnNovaClasse" type="button" class="btn btn-primary" aria-label="Left Align" >
                    <i class="fas fa-plus-circle"></i> Nova Classe
                </button>
            </div>
        </div>
    <br/>
    <div class="row" >
        <div class="col panel panel-primary table-responsive">

                <table id="tbClasses" class="table table-hover text-left">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Descrição</th>
                            <th>Admin</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($classes as $cls)
                            <tr>
                                <td id="{{$cls->getId()}}_codigo">{{$cls->getCodigo()}}</td>
                                <td id="{{$cls->getId()}}_descricao">{{$cls->getDescricao()}}</td>
                                <td id="{{$cls->getId()}}_admin">

                                    @if($cls->isAdmin())
                                        <span class="badge badge-success">Sim</span>
                                    @else
                                        <span class="badge badge-danger">Não</span>
                                    @endif


                                </td>
                                <td>
                                    <span name="editBtn" class="fas fa-edit" data="{{$cls->getId()}}" style="cursor: pointer;color: #009933;" title="Editar"></span>
                                    <span name="delBtn" class="fas fa-trash-alt" data="{{$cls->getId()}}" style="cursor: pointer;color: #ff0000;" title="Excluir"></span>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Nova Classe',
		'icone'=>'fa-user-tag',
        'rota' => 'adm.classe.salvar',
        'campos'=>
        [
            [
                'id' =>'txtCodigo',
                'label'=>'Código',
                'nome'=>'codigo',
                'tamanho'=>'5',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'txtDescricao',
                'label'=>'Descrição',
                'nome'=>'descricao',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ]
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.classe.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir a Classe'
    ])

@endsection
