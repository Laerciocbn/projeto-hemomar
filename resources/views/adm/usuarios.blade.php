@extends('layouts.app')

@section('titulo')
    <i class="fas fa-user"></i> Cadastro de Usuários
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/adm/usuario.js') }}}"></script>
@endsection

@section('pagina')


    <div class="container-fluid">

        <div class="row" >
            <div class="col text-left">
                <button id="btnNovoUsuario" type="button" class="btn btn-primary" aria-label="Left Align" >
                    <i class="fas fa-plus-circle"></i> Novo Usuário
                </button>
            </div>
        </div>
        <br/>
        <div class="row" >
            <div class="col panel panel-primary table-responsive">

                <table id="tbUsuarios" class="table table-hover text-left">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>E-mail</th>
                            <th>Telefone</th>
                            <th>Celular</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($usuarios as $usu)
                            <tr>
                                <td id="{{$usu->getId()}}_nome">{{$usu->getNome()}}</td>
                                <td id="{{$usu->getId()}}_cpf">{{$usu->getCpf()}}</td>
                                <td id="{{$usu->getId()}}_email">{{$usu->getEmail()}}</td>
                                <td id="{{$usu->getId()}}_telefone">{{$usu->getTelefone()}}</td>
                                <td id="{{$usu->getId()}}_celular">{{$usu->getCelular()}}</td>
                                <td id="{{$usu->getId()}}_ativo" data="{{$usu->getAtivo()}}">
                                    @if($usu->getAtivo())
                                        <span class="badge badge-success">Ativo</span>
                                    @else
                                        <span class="badge badge-danger">Inativo</span>
                                    @endif
                                </td>
                                <td>
                                    <span name="editBtn" class="cursor-pointer fas fa-edit" data="{{$usu->getId()}}" style="color: #009933;" title="Editar"></span>
                                    <span name="delBtn" class="cursor-pointer fas fa-trash-alt" data="{{$usu->getId()}}" style="color: #ff0000;" title="Excluir"></span>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Novo Usuário',
        'rota' => 'adm.usuario.salvar',
        'icone' => 'fa-user',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'5',
                        'padding'=>'50',
                        'id' =>'txtCpf',
                        'label'=>'CPF',
                        'nome'=>'cpf',
                        'classe'=>'mask_cpf',
                        'tamanho'=>'11',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true
                    ],
                    [
                        'largura'=>'4',
                        'id' =>'cbAtivo',
                        'label'=>'Ativo',
                        'nome'=>'ativo',
                        'tipo'=>'slider',
                        'required'=> true
                    ],
                ]
            ],
            [
                'id' =>'txtNome',
                'label'=>'Nome',
                'nome'=>'nome',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'txtEmail',
                'label'=>'E-mail',
                'nome'=>'email',
                'tamanho'=>'100',
                'tipo'=>'email',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'6',
                        'id' =>'txtTelefonne',
                        'label'=>'Telefone',
                        'nome'=>'telefone',
                        'classe'=>'mask_telefone',
                        'tamanho'=>'15',
                        'tipo'=>'txt',
                        'autocomplete'=>'off',
                        'required'=> false,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'6',
                        'id' =>'txtCelular',
                        'label'=>'Celular',
                        'nome'=>'celular',
                        'classe'=>'mask_celular',
                        'tamanho'=>'10',
                        'tipo'=>'txt',
                        'autocomplete'=>'off',
                        'required'=> false,
                        'disabled'=>false
                    ]
                ]
            ],
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'6',
                        'id' =>'txtSenha',
                        'label'=>'Senha',
                        'nome'=>'senha',
                        'tamanho'=>'50',
                        'tipo'=>'password',
                        'required'=> true,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'6',
                        'id' =>'txtConfirmSenha',
                        'label'=>'Confirmar senha',
                        'nome'=>'confirma_senha',
                        'tamanho'=>'50',
                        'tipo'=>'password',
                        'required'=> true,
                        'disabled'=>false
                    ]
                ]
            ],
            /*
            [
                'id' =>'cbClasse',
                'label'=>'Nível de acesso',
                'nome'=>'classe',
                'tipo'=>'combo',
                'opcoes'=>$classes,
                'required'=> true,
                'disabled'=>true
            ],
            */
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.usuario.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir o Usuário'
    ])

@endsection
