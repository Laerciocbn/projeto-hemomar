@extends('layouts.app')

@section('titulo')
    <i class="fas fa-key"></i> Permissões
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/adm/permissao.js') }}}"></script>
@endsection

@section('pagina')
    
    <div class="container-fluid">
        <div class="row" >
            <div class="col text-left">
                <button id="btnNovoParametro" type="button" class="btn btn-primary" aria-label="Left Align" >
                    <i class="fas fa-plus-circle"></i> Nova Permissão
                </button>
            </div>
        </div>
    <br/>
    <div class="row" >
        <div class="col panel panel-primary table-responsive">

                <table id="tbParametros" class="table table-hover text-left">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($parametros as $param)
                            <tr>
                                <td id="{{$param->getId()}}_codigo">{{$param->getCodigo()}}</td>
                                <td id="{{$param->getId()}}_descricao">{{$param->getDescricao()}}</td>
                                <td id="{{$param->getId()}}_valor">{{$param->getValor()}}</td>
                                <td>
                                    <span name="editBtn" class="fas fa-edit" data="{{$param->getId()}}" style="cursor: pointer; color: #009933;" title="Editar"></span>
                                    <span name="delBtn" class="fas fa-trash-alt" data="{{$param->getId()}}" style="cursor: pointer;color: #ff0000;" title="Excluir"></span>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Novo Parâmetro',
		'icone'=>'fa-cog',
        'rota' => 'adm.parametro.salvar',
        'campos'=> 
        [
            [
                'id' =>'txtCodigo',
                'label'=>'Código',
                'nome'=>'codigo',
                'tamanho'=>'6',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'txtDescricao',
                'label'=>'Descrição',
                'nome'=>'descricao',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'id' =>'txtValor',
                'label'=>'Valor',
                'nome'=>'valor',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ]
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'adm.parametro.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir o Parâmetro'
    ])

@endsection