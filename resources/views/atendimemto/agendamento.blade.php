@extends('layouts.app')

@section('titulo')
 <i class="fa fa-calendar"></i> Agendamento   
@endsection




@section('css')
    <link rel="stylesheet" href="{{{ URL::asset('css/bootstrap-select-1.13.2/bootstrap-select.min.css') }}}" />
    <link rel="stylesheet" href="{{{ URL::asset('css/tempus-dominus/tempusdominus-bootstrap-4.min.css') }}}" />
    <link rel="stylesheet" href="{{ URL::asset('fullcalendar/packages/core/main.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('fullcalendar/packages/daygrid/main.css') }}" />
@endsection

@section('scripts')

    <script src="{{{ URL::asset('js/bootstrap-select-1.13.2/bootstrap-select.min.js') }}}"></script>
    <script src="{{{ URL::asset('js/bootstrap-select-1.13.2/i18n/defaults-pt_BR.min.js') }}}"></script>

    <script src="{{{ URL::asset('js/moment/moment.js') }}}"></script>
    <script src="{{{ URL::asset('js/moment/locale/pt-br.js') }}}"></script>
    <script src="{{{ URL::asset('js/tempus-dominus/tempusdominus-bootstrap-4.min.js') }}}"></script>

    <script src="{{{ URL::asset('js/views/chamado/meus_chamados.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/chamado/common_chamados.js') }}}"></script>


    <script src="{{{ URL::asset('fullcalendar/packages/core/locales/pt-br.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/calendario.js') }}}"></script>
    <script src="{{{ URL::asset('js/views/agendamento.js') }}}"></script>
    <script src="{{{ URL::asset('fullcalendar/packages/core/main.js') }}}"></script>
    <script src="{{{ URL::asset('fullcalendar/packages/interaction/main.js') }}}"></script>
    <script src="{{{ URL::asset('fullcalendar/packages/daygrid/main.js') }}}"></script>

@endsection

@section('pagina')


<div class="container-fluid">
    <div class="row" >
        <div class="col-md-3">
                <div class="col-12" style="min-height: 100px;">
                    </div>
            <div class="col-12">
                <button type="button" class="w-100 btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Painel de chamada
                </button>
            </div>
            <br><br>
            <div class="col-12">
                <button id="btnNovoAgendamento" type="button" class="w-100 btn btn-primary" aria-label="Left Align" >
                    Novo Agendamento 
                </button>
            </div>
            <br>
            <div>
                   
            </div><!-- /col-md-3 -->
           <br><br>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Eventos</h3>
                </div>
                <div class="box-body">
                    <div class="btn-group" style="width: 100%; margin-bottom: 10px;">




                        <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                        
                        <button type="button" class="btn btn-primary"></button>

                        <button type="button" class="btn btn-secondary"></button>
                        
                        <button type="button" class="btn btn-success"></button>
                        
                        <button type="button" class="btn btn-danger"></button>
                        <button type="button" class="btn btn-warning"></button>
                        <button type="button" class="btn btn-info"></button>
                        <button type="button" class="btn btn-light"></button>
                        <button type="button" class="btn btn-dark"></button>

                        
                        
                    </div>
                    <!-- /btn-group -->
                    <div class="input-group">
                        <input id="new-event" type="text" class="form-control" placeholder="busca evento">
      
                        <div class="input-group-btn">
                            <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
                        </div>
                        <!-- /btn-group -->
                    </div>
                    <!-- /input-group -->
                </div>
            </div>
        </div><!-- /.col -->

     
        <div class="col-md-9">
            <div id='calendar'></div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-tv"></i> Painel de Chamado</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row"> {{date('d/m/y h:i:s A')}}
                    <div class="col-sm-12 mt-3">
                        <div class="row">
                            <div class="col-8 col-sm-6">
                                Senha: <h4>009</h4>
                                Prioridade: <h4>Normal</h4>
                                Servidor: <h4>Laercio</h4>
                            </div>
                            <div class="col-4 col-sm-6">
                                <div class="col-12 text-center">
                                    <label>Atendidos</label>
                                    <h4>5</h4>
                                </div>
                                <div class="col-12 text-center">
                                   <label>Guiche</label>
                                   <h4>2</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-4 text-center">  
                <button type="button" class="btn btn-info btn-lg " data-dismiss="modal"><i class="fas fa-bullhorn"></i></button>
            </div>
            <div class="col-4 text-center">
                <button type="button" class="btn btn-success btn-lg " data-dismiss="modal"><i class="fas fa-thumbs-up"></i></button>
            </div>
            <div class="col-4 text-right">
                <button type="button" class="btn btn-danger btn-lg " data-dismiss="modal"><i class="fas fa-plus-circle"></i></button>
            </div>
        </div>
    </div>
  </div>

</div>



    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Novo Agendamento',
        'rota' => 'atendomento.agendamento.salvar',
        'icone' => 'fa-user',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'4',
                        'id' =>'cbAtivo',
                        'label'=>'Ativo',
                        'nome'=>'ativo',
                        'tipo'=>'slider',
                        'required'=> true
                    ],
                ]
            ],
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'id' =>'cbEstado',
                        'label'=>'Estado',
                        'nome'=>'uf',
                        'tipo'=>'combo',
                        'opcoes'=> $fk_convenio,
                        'default'=>'Selecione',
                        'required'=> true, 
                        'autofocus'=> false,
                        'disabled'=>false
                    ],
                    [
                        'id' =>'cbEstado',
                        'label'=>'Estado',
                        'nome'=>'uf',
                        'tipo'=>'combo',
                        'opcoes'=> $fk_paciente,
                        'default'=>'Selecione',
                        'required'=> true, 
                        'autofocus'=> false,
                        'disabled'=>false
                    ]
                ]
            ],
            [
                'id' =>'txTitulo',
                'label'=>'Titulo',
                'nome'=>'titulo',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'6',
                        'id' =>'txtDtInicio',
                        'label'=>'Inicio',
                        'nome'=>'dt_inicio',
                        'tamanho'=>'15',
                        'tipo'=>'txt',
                        'autocomplete'=>'off',
                        'required'=> false,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'6',
                        'id' =>'txtDtFim',
                        'label'=>'Data Fim',
                        'nome'=>'dt_fim',
                        'tamanho'=>'10',
                        'tipo'=>'txt',
                        'autocomplete'=>'off',
                        'required'=> false,
                        'disabled'=>false
                    ]
                ]
            ],
            [
                'id' =>'txObs',
                'label'=>'Obs',
                'nome'=>'obs',
                'tamanho'=>'200',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],
            /*
            [
                'id' =>'cbClasse',
                'label'=>'Nível de acesso',
                'nome'=>'classe',
                'tipo'=>'combo',
                'opcoes'=>$classes,
                'required'=> true,
                'disabled'=>true
            ],
            */
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'cadastro.medico.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir o Médico'
    ])

@endsection





