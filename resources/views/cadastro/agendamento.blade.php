@extends('layouts.app')

@section('titulo')
    <i class="far fa-calendar-check"></i> Agendamento
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/cadastros/agendamento.js') }}}"></script>
@endsection

@section('pagina')


    <div class="container-fluid">

        <div class="row" >
            <div class="col text-left">
                <button id="btnNovoAgendamento" type="button" class="btn btn-primary" aria-label="Left Align" >
                    <i class="fas fa-file-medical"></i> Novo Agendamento
                </button>
            </div>
        </div>
        <br/>
        <div class="row" >
            <div class="col panel panel-primary table-responsive">

                <table id="tbAgendamento" class="table table-hover text-left">
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Código</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($especialidades as $especialidade)
                            <tr>
                                <td id="{{$especialidade->getId()}}_descricao">{{$especialidade->getDescricao()}}</td>
                                @if($especialidade->getAtuacao()=='')
                                    <td id="{{$especialidade->getId()}}_atuacao">Sem área de atuação</td>
                                @else
                               
                                    <td id="{{$especialidade->getId()}}_atuacao">{{$especialidade->getAtuacao()}}</td>
                                @endif                    
                                
                                <td id="{{$especialidade->getId()}}_ativo" data="{{$especialidade->getAtivo()}}">
                                    @if($especialidade->getAtivo())
                                        <span class="badge badge-success">Ativo</span>
                                    @else
                                        <span class="badge badge-danger">Inativo</span>
                                    @endif
                                </td>
                                <td>
                                    <span name="editBtn" class="cursor-pointer fas fa-edit" data="{{$especialidade->getId()}}" style="color: #009933;" title="Editar"></span>
                                    <span name="delBtn" class="cursor-pointer fas fa-trash-alt" data="{{$especialidade->getId()}}" style="color: #ff0000;" title="Excluir"></span>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Nova Especialidade',
        'rota' => 'cadastro.especialidade.salvar',
        'icone' => 'far fa-file-medical',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'4',
                        'id' =>'cbAtivo',
                        'label'=>'Ativo',
                        'nome'=>'ativo',
                        'tipo'=>'slider',
                        'required'=> true
                    ],
                ]
            ],
            [
                'id' =>'txtDescricao',
                'label'=>'Descrição',
                'nome'=>'descricao',
                'largura'=>'5',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],   
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'5',
                        'padding'=>'50',
                        'id' =>'txtAtuacao',
                        'label'=>'Atuação',
                        'nome'=>'atuacao',
                        'tamanho'=>'11',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true
                    ],
                ]
            ]
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'cadastro.especialidade.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir a Especialidade'
    ])

@endsection
