@extends('layouts.app')

@section('titulo')
    <i class="fas fa-user-md"></i> Cadastro de Médico
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/cadastros/medico.js') }}}"></script>
@endsection

@section('pagina')


    <div class="container-fluid">

        <div class="row" >
            <div class="col text-left">
                <button id="btnNovoMedico" type="button" class="btn btn-primary" aria-label="Left Align" >
                    <i class="fas fa-user-md"></i> Novo Médico
                </button>
            </div>
        </div>
        <br/>
        <div class="row" >
            <div class="col panel panel-primary table-responsive">

                <table id="tbMecicos" class="table table-hover text-left">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>CRM</th>
                            <th>E-mail</th>
                            <th>Telefone</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($medicos as $medico)
                            <tr>
                                <td id="{{$medico->getId()}}_nome">{{$medico->getNome()}}</td>
                                <td id="{{$medico->getId()}}_cpf">{{$medico->getCpf()}}</td>
                                <td id="{{$medico->getId()}}_crm">{{$medico->getCrm()}}</td>
                                <td id="{{$medico->getId()}}_email">{{$medico->getEmail()}}</td>
                                <td id="{{$medico->getId()}}_telefone">{{$medico->getTelefone()}}</td>
                                <td id="{{$medico->getId()}}_ativo" data="{{$medico->getAtivo()}}">
                                    @if($medico->getAtivo())
                                        <span class="badge badge-success">Ativo</span>
                                    @else
                                        <span class="badge badge-danger">Inativo</span>
                                    @endif
                                </td>
                                <td>
                                    <span name="editBtn" class="cursor-pointer fas fa-edit" data="{{$medico->getId()}}" style="color: #009933;" title="Editar"></span>
                                    <span name="delBtn" class="cursor-pointer fas fa-trash-alt" data="{{$medico->getId()}}" style="color: #ff0000;" title="Excluir"></span>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Novo Médico',
        'rota' => 'cadastro.medico.salvar',
        'icone' => 'fas fa-user-md',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'4',
                        'id' =>'cbAtivo',
                        'label'=>'Ativo',
                        'nome'=>'ativo',
                        'tipo'=>'slider',
                        'required'=> true
                    ],
                ]
            ],
            [
                'id' =>'txtNome',
                'label'=>'Nome',
                'nome'=>'nome',
                'largura'=>'5',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],   
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'5',
                        'padding'=>'50',
                        'id' =>'txtCpf',
                        'label'=>'CPF',
                        'nome'=>'cpf',
                        'classe'=>'mask_cpf',
                        'tamanho'=>'11',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true
                    ],
                    [
                        'largura'=>'5',
                        'id' =>'txtCrm',
                        'label'=>'CRM',
                        'nome'=>'crm',
                        'tamanho'=>'5',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                ]
            ],  
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'8',
                        'id' =>'txtEmail',
                        'label'=>'E-mail',
                        'nome'=>'email',
                        'tamanho'=>'100',
                        'tipo'=>'email',
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'4',
                        'id' =>'txtTelefone',
                        'label'=>'Telefone',
                        'nome'=>'telefone',
                        'tamanho'=>'15',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                ]
            ],
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'cadastro.medico.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir o Usuário'
    ])

@endsection
