@extends('layouts.app')

@section('titulo')
    <i class="fas fa-procedures"></i> Cadastro de Paciente
@endsection

@section('scripts')
    <script src="{{{ URL::asset('js/views/cadastros/paciente.js') }}}"></script>
@endsection

@section('pagina')


    <div class="container-fluid">

        <div class="row" >
            <div class="col text-left">
                <button id="btnNovoPaciente" type="button" class="btn btn-primary" aria-label="Left Align" >
                    <i class="fas fa-user-md"></i> Novo Paciente
                </button>
            </div>
        </div>
        <br/>
        <div class="row" >
            <div class="col panel panel-primary table-responsive">

                <table id="tbPacientes" class="table table-hover text-left">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>E-mail</th>
                            <th>Telefone</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($pacientes as $paciente)
                            <tr>
                                <td id="{{$paciente->getId()}}_nome">{{$paciente->getNome()}}</td>
                                <td id="{{$paciente->getId()}}_cpf">{{$paciente->getCpf()}}</td>
                                <td id="{{$paciente->getId()}}_email">{{$paciente->getEmail()}}</td>
                                <td id="{{$paciente->getId()}}_telefone">{{$paciente->getTelefone()}}</td>
                                <td id="{{$paciente->getId()}}_ativo" data="{{$paciente->getAtivo()}}">
                                    @if($paciente->getAtivo())
                                        <span class="badge badge-success">Ativo</span>
                                    @else
                                        <span class="badge badge-danger">Inativo</span>
                                    @endif
                                </td>
                                <td>
                                    <span name="editBtn" class="cursor-pointer fas fa-edit" data="{{$paciente->getId()}}" style="color: #009933;" title="Editar"></span>
                                    <span name="delBtn" class="cursor-pointer fas fa-trash-alt" data="{{$paciente->getId()}}" style="color: #ff0000;" title="Excluir"></span>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Inclui formulário modal -->
    @include('partials._modal_form',
    [
        'titulo'=> 'Novo Paciente',
        'rota' => 'cadastro.paciente.salvar',
        'icone' => 'fas fa-user-md',
        'campos'=>
        [
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'4',
                        'id' =>'cbAtivo',
                        'label'=>'Ativo',
                        'nome'=>'ativo',
                        'tipo'=>'slider',
                        'required'=> true
                    ],
                ]
            ],
            [
                'id' =>'txtNome',
                'label'=>'Nome',
                'nome'=>'nome',
                'largura'=>'5',
                'tamanho'=>'50',
                'tipo'=>'txt',
                'required'=> true,
                'autofocus'=> true,
                'disabled'=>false
            ],   
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'5',
                        'padding'=>'50',
                        'id' =>'txtCpf',
                        'label'=>'CPF',
                        'nome'=>'cpf',
                        'classe'=>'mask_cpf',
                        'tamanho'=>'11',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true
                    ],
                ]
            ],  
            [
                'tipo'=>'array',
                'campos'=>
                [
                    [
                        'largura'=>'8',
                        'id' =>'txtEmail',
                        'label'=>'E-mail',
                        'nome'=>'email',
                        'tamanho'=>'100',
                        'tipo'=>'email',
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                    [
                        'largura'=>'4',
                        'id' =>'txtTelefone',
                        'label'=>'Telefone',
                        'nome'=>'telefone',
                        'tamanho'=>'15',
                        'tipo'=>'txt',
                        'required'=> true,
                        'autofocus'=> true,
                        'disabled'=>false
                    ],
                ]
            ],
        ]
    ])

    @include('partials._modal_delete',
    [
        'rota'=>'cadastro.paciente.delete',
        'titulo'=>'Confirma Exclusão',
        'mensagem_delete'=>'Tem certeza que deseja excluir o paciente'
    ])

@endsection
