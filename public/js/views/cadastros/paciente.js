
$("#btnNovoPaciente").click(function()
{
    LimparCampos();

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});


$("#tbPacientes tbody").on("click","span[name=editBtn]",function()
{
    var id = $(this).attr("data");

    LimparCampos();

    $('#titulo_form_modal').text("Atualizar paciente");
    $('#id_form_modal').val(id);
    $('#txtNome').val($("#" + id + "_nome").text());
    $('#txtCpf').val($("#" + id + "_cpf").text());
    $('#txtEmail').val($("#" + id + "_email").text());
    $('#txtTelefone').val($("#" + id + "_telefone").text());
    $('#cbAtivo').prop('checked',$("#" + id + "_ativo").attr('data')==1);

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#tbPaciente tbody").on("click","span[name=editBtn]",function()
{
    var id = $(this).attr("data");

    LimparCampos();

    $('#titulo_form_modal').text("Atualizar paciente");
    $('#id_form_modal').val(id);
    $('#txtNome').val($("#" + id + "_nome").text());
    $('#txtCpf').val($("#" + id + "_cpf").text());
    $('#txtEmail').val($("#" + id + "_email").text());
    $('#txtTelefone').val($("#" + id + "_telefone").text());
    $('#cbAtivo').prop('checked',$("#" + id + "_ativo").attr('data')==1);
    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#tbPacientes tbody").on("click","span[name=delBtn]",function()
{
    var id = $(this).attr("data");

    $('#id_delete_modal').val(id);
    $('#item_delete').text($("#" + id + "_nome").text());
    $("#dlgDelete").modal({backdrop: "static"}).show();
});

function LimparCampos()
{
    $('#titulo_form_modal').text("Novo paciente");

    $('#cbAtivo').prop('checked',true);
    $("#txtNome").prop('disabled',false);
    $("#txtCpf").prop('disabled',false);
    $("#txtEmail").prop('disabled',false);

    $('#id_form_modal').val('');
	$('#txtNome').val('');
	$('#txtEndereco').val('');
}

