
$("#btnNovoEspecialidade").click(function()
{
    LimparCampos();

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});


$("#tbEspecialidades tbody").on("click","span[name=editBtn]",function()
{
    var id = $(this).attr("data");

    LimparCampos();

    $('#titulo_form_modal').text("Atualizar Médico");
    $('#id_form_modal').val(id);
    $('#txtDescricao').val($("#" + id + "_descricao").text());
    $('#txtAtuacao').val($("#" + id + "_atuacao").text());
    $('#cbAtivo').prop('checked',$("#" + id + "_ativo").attr('data')==1);

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#tbEspecialidades tbody").on("click","span[name=editBtn]",function()
{
    var id = $(this).attr("data");

    LimparCampos();

    $('#titulo_form_modal').text("Atualizar Especialidade");
    $('#id_form_modal').val(id);
    $('#txtDescricao').val($("#" + id + "_descricao").text());
    $('#txtAtuacao').val($("#" + id + "_atuacao").text());
    $('#cbAtivo').prop('checked',$("#" + id + "_ativo").attr('data')==1);
    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#tbEspecialidades tbody").on("click","span[name=delBtn]",function()
{
    var id = $(this).attr("data");

    $('#id_delete_modal').val(id);
    $('#item_delete').text($("#" + id + "_nome").text());
    $("#dlgDelete").modal({backdrop: "static"}).show();
});

function LimparCampos()
{
    $('#titulo_form_modal').text("Novo Especialidade");

    $('#cbAtivo').prop('checked',true);
    $("#txtDescricao").prop('disabled',false);
    $("#txtAtuacao").prop('disabled',false);
    $('#id_form_modal').val('');
	
}

