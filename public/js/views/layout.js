$(window).on('load',function()
{
	splashOut();
});

$(window).on('beforeunload',function()
{
	splashIn();
});

$(document).ready(function ()
{
    abreMenu();
});

$('#sidebarCollapse').on('click', function ()
{
    $('#sidebar').toggleClass('active');
});

//ativa a propriedade maxlength para limitar o conteúdo do campo
$("input[type='text'], textarea").maxlength(
{
    text: ''
});

//máscaras dos campos via classe
$(".mask_cpf").mask('000.000.000-00',{reverse:true,placeholder:"___.___.___-__"});
$('.mask_cnpj').mask('00.000.000/0000-00', {reverse: true,placeholder:"__.___.___/____-__"});
$(".mask_cep").mask('00000-000',{reverse:true,placeholder:"_____-___"});
$(".mask_celular").mask('(00) 00000-0000',{reverse:false,placeholder:"(__) _____-____"});
$(".mask_telefone").mask('(00) 0000-0000',{reverse:false,placeholder:"(__) ____-____"});
$(".mask_placa").mask('SSS-0000',{reverse:false,placeholder:"___-____"});

/* ----------------------------------- FUNÇÕES ----------------------------------- */

function MensagemBox(tipo,titulo,mensagem)
{
    var icone = '' ;
    var estilo = $('#header_modal_mensagem');

    switch(tipo)
    {
        case 'sucesso':
            classe = 'alert alert-success text-center';
            icone = 'fas fa-check-circle';
            estilo.css({'background-color':'green','color':'white'});
            break;
        case 'alerta':
            classe = 'alert alert-warning text-center';
            icone = 'fas fa-exclamation-triangle';
            estilo.css({'background-color':'orange','color':'white'});
            break;
        case 'alerta-vermelho':
            classe = 'alert alert-danger text-center';
            icone = 'fas fa-exclamation-triangle';
            estilo.css({'background-color':'red','color':'white'});
            break;
        case 'erro':
            classe = 'alert alert-danger text-center';
            icone = 'fas fa-times-circle';
            estilo.css({'background-color':'red','color':'white'});
            break;
        case 'info':
            classe = 'alert alert-info text-center';
            icone = 'fas fa-info-circle';
            estilo.css({'background-color':'blue','color':'white'});
            break;
    }

    $('#icon_modal_mensagem').attr('class',icone);
    $('#titulo_modal_mensagem').text(titulo);
    $('#msg_modal_mensagem').text(mensagem);


    $("#dlgMensagem").modal({backdrop: "static"}).show();
}

function Mensagem(tipo,mensagem)
{
    var icone = '';
    var classe = '';

    switch(tipo)
    {
        case 'sucesso':
            classe = 'alert alert-success text-center';
            icone = 'fas fa-check-circle';
         break;
        case 'alerta':
            classe = 'alert alert-warning text-center';
            icone = 'fas fa-exclamation-triangle';
         break;
        case 'alerta-vermelho':
            classe = 'alert alert-danger text-center';
            icone = 'fas fa-exclamation-triangle';
        break;
        case 'erro':
            classe = 'alert alert-danger text-center';
            icone = 'fas fa-times-circle';
         break;
        case 'info':
            classe = 'alert alert-info text-center';
            icone = 'fas fa-info-circle';
         break;
    }

    var alerta = '<div class="'+ classe +' alert-dismissible fade show" role="alert">'
               +    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
               +        '<span aria-hidden="true">&times;</span>'
               +    '</button>'
               +    '<i class="'+ icone +'" style="font-size:24px"></i> '+ mensagem
               + '</div>'


    $('#Mensagens').append(alerta);
}

function ModalAjaxCarregando(ativo,componente,texto)
{
    if(ativo)
    {
        $('<div id="' + componente.attr('id') + 'ajaxModal" class="modal " style="position:absolute !important;" role="dialog">'
        +    '<div class="modal-dialog">'
        +        '<div class="modal-content">'
        +           '<div class="modal-body">'
        +               '<div class="row">'
        +                   '<div class="col-md-2">'
        +                       '<div class="lds-dual-ring"></div>'
        +                   '</div>'
        +                   '<div class="col-md-9">'
        +                       '<h2>' + texto + '</h2>'
        +                   '</div>'
        +             '</div>'
        +          '</div>'
        +      '</div>'
        +   '</div>'
        +' </div>').appendTo('#' + componente.attr('id'));

        $('#' + componente.attr('id') + 'ajaxModal').modal("show");
    }
    else
    {
        $('#' + componente.attr('id') + 'ajaxModal').modal("hide");
        $('#' + componente.attr('id') + 'ajaxModal').remove();
    }
}

function isNullVazio(item)
{
    if(item == null)
    {
        return '';
    }
    else
    {
        return item;
    }

}

function linhaNenhumResultado(colunas,texto=null,tag='h2')
{
   return  '<tr>'
         +     '<td colspan="'+ colunas +'" class="text-center">'
         +         '<'+ tag +'><br/>'+ ((texto != null) ? texto : 'Nenhum resultado encontrado' ) +'</'+ tag +'>'
         +     '</td>'
         + '</tr>';
}

function formataData(data = null,horario = false)
{
    var date = (data == null ? new Date() : data);
    var dia = addZero(date.getDate()),
        mes = addZero(date.getMonth() + 1),
        ano = date.getFullYear(),
        hora = addZero(date.getHours()),
        minuto = addZero(date.getMinutes()),
        segundo = addZero(date.getSeconds());


    var resultado = dia + '/' + mes + '/' + ano;

    if(horario)
    {
        resultado += (' ' + hora + ':' + minuto + ':'+ segundo)
    }


    return resultado;
}

function addZero(numero)
{
    if (numero < 10)
    {
        numero = '0' + numero;
    }

    return numero;
}

function splashOut()
{
	$(".se-pre-con").fadeOut("slow");
}

function splashIn()
{
	$(".se-pre-con").fadeIn("slow");
}

function Popover(elemento,mensagem,classe='text-danger',lugar = 'top')
{
    elemento.popover(
    {
        placement:lugar,
        container:'body',
        html:true,
        content:'<b class="'+ classe +'">'+ mensagem +'</b>'
    });

    elemento.popover('show');
}

function abreMenu()
{
    var menu = $("#sidebar a[href='"+ window.location.href + "']").first();

    menu.parent().parent().collapse()
}

function ResizeModal(modal)
{
    modal.on('shown.bs.modal',function()
    {
        $(this).find('.modal-dialog').css(
        {
            width:'auto',
            height:'auto',
            'max-height':'100%'
        });

        $(this).find('.modal-body').css(
        {
            width:'auto',
            height:'auto',
            'overflow-y':'auto',
            'max-height':'100%',
            'padding-bottom':'20%'
        });
    });
}
