
$("#btnNovoUsuario").click(function()
{
    LimparCampos();

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#btnPesquisaUsuario").click(function()
{
    PesquisaUsuario
    (
        $("#txtNomePesquisa").val(),
        $("#txtCpfPesquisa").val(),
        $("#txtTipoPesquisa").val(),
        $("#txtEmpresaPesquisa").val(),
        $("#txtStatusPesquisa").val()
    );
});

$("#tbUsuario tbody").on("click","span[name=editBtn]",function()
{
    var id = $(this).attr("data");

    LimparCampos();

    $('#titulo_form_modal').text("Atualizar Usuário");
    $('#id_form_modal').val(id);
    $('#txtNome').val($("#" + id + "_nome").text());
    $('#cbAtivo').prop('checked',$("#" + id + "_ativo").attr('data')==1);

    $("#dlgFormulario").modal({backdrop: "static"}).show();
});

$("#tbUsuario tbody").on("click","span[name=delBtn]",function()
{
    var id = $(this).attr("data");

    $('#id_delete_modal').val(id);
    $('#item_delete').text($("#" + id + "_nome").text());
    $("#dlgDelete").modal({backdrop: "static"}).show();
});

$("#sldTerceiro").change(function()
{
    if(!($(this).prop('checked')))
    {
        $("#cbTerceiro").val('');
    }

    $("#cbTerceiro").prop('disabled',!($(this).prop('checked')));
    $("#txtNome").prop('disabled',!($(this).prop('checked')));
    $("#txtEmail").prop('disabled',!($(this).prop('checked')));
});



function LimparCampos()
{
    $('#titulo_form_modal').text("Novo Usuário");

    $('#sldTerceiro').prop('checked',true);
    $('#cbAtivo').prop('checked',true);

    $("#cbTerceiro").prop('disabled',false);
    $("#txtNome").prop('disabled',false);
    $("#txtEmail").prop('disabled',false);

    $('#id_form_modal').val('');
	$('#txtNome').val('');
	$('#txtEndereco').val('');
}

function PesquisaUsuario(nomePesq,cpfPesq,tipoPesq,empresaPesq,statusPesq)
{
    $.ajaxSetup(
    {
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax(
    {
        url: "ajax-pesquisa-usuario",
        method: 'post',
        data:
        {
            nome: nomePesq,
            cpf: cpfPesq,
            tipo: tipoPesq,
            empresa: empresaPesq,
            status: statusPesq
        },
        beforeSend: function()
        {
			$("#tbUsuario tbody tr").remove();
            
            ModalAjaxCarregando(true,$("#pgUsuario"),'Carregando...');
        },
        complete: function()
        {
            ModalAjaxCarregando(false,$("#pgUsuario"),null);
        },
        success: function(data)
        {
            $("#tbUsuario").html(data);

            /*
            data.forEach(item =>
            {
                $("#tbUsuario tbody").append(linhaTabelaUsuario(item));
            });
            */
        },
        error:function(error)
        {
            console.log(error);

            MensagemBox('erro','Erro',(error.responseJSON.exception != null && error.responseJSON.exception != '' ?  error.responseJSON.exception : error.responseJSON));
        }
    });
}

function linhaTabelaUsuario(item)
{
    var linha = '<tr>'
              + '<td id="'+ item.id +'_usuario">'+ item.nome +'</td>'
              + '<td id="'+ item.id +'_cpf">'+ item.cpf +'</td>'
              + '<td id="'+ item.id +'_tipo">'+ item.tipo +'</td>'
              + '<td id="'+ item.id +'_classe" data="'+ item.classe.id +'">'+ item.classe.codigo +'</td>'
              + '<td id="'+ item.id +'_terceiro" data="'+ item.terceiro.id +'">'+ item.terceiro.descricao +'</td>'
              + '<td id="'+ item.id +'_ativo" data="'+ item.ativo +'">';
    
    if(item.ativo)
    {
        linha += '<span class="badge badge-success">Ativo</span>';
    }
    else
    {
        linha += '<span class="badge badge-danger">Inativo</span>';
    }
    
    linha += '</td>'
          +  '<td>'
          +    '<span name="editBtn" data="'+ item.id +'" class="fas fa-edit botaoeditar cursor-pointer" title="Editar"></span>'
          +    '<span name="delBtn" data="'+ item.id +'" class="fas fa-trash-alt botaoexcluir cursor-pointer" title="Excluir"></span>'
          +  '</td>'
          +'</tr>';
}
