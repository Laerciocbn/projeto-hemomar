$(function ()
{
    $('#txtData').datetimepicker(
        {
            format:'D/M/YYYY'
        });

});

$(document).ready(function()
{
    $('#btnNovoAgendamento').click(function()
    {
        LimparCampos();
        
        $('#titulo_form_modal').text("Novo Agendamento");

        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });


    $("#tbAgendamento tbody").on("click","span[name=editBtn]",function()
    {
        LimparCampos();
        
        $('#titulo_form_modal').text("Atualizar Agendanemto");

        var id = $(this).attr("data");

        $('#id_form_modal').val(id);
        $('#txtTitulo').val($("#" + id + "_titulo").text());
        $('#txtInicio').val($("#" + id + "_inicio").text());
        $('#txtFim').val($("#" + id + "_fim").text());
        $('#txtObs').val($("#" + id + "_obs").text());
        
        $("#dlgFormulario").modal({backdrop: "static"}).show();
    });


    $("#tbAgendamento tbody").on("click","span[name=delBtn]",function()
    {
        var id = $(this).attr("data");

        $('#id_delete_modal').val(id);
        $('#item_delete').text($("#" + id + "_codigo").text());
        $("#dlgDelete").modal({backdrop: "static"}).show();
    });
});

  function LimparCampos()
  {
  $('#id_form_modal').val('');
  $('#txTitulo').val('');
  $('#txtDtInicio').val('');
  $('#txtDtFim').val('');
  $('#txObs').val('');
     
  }