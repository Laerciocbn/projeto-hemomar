
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      locale: 'pt-br',
      defaultDate: Date(),
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
           
            

        }
      ]
    });

    calendar.render();
  });