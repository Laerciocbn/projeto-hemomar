USE DB_MODELO;

/*CODIGO PADRÃO DO SISTEMA: MODEL */

/* CRIA USUARIO ADMIN SENHA: Hond@crf450x */
INSERT INTO TBG_usuario(USUA_nome,USUA_cpf,password,USUA_email,USUA_ativo) VALUES('Admin','12345678910','$2y$10$.64eKbnY2N2gCL2UbbbTA.SnT4RUQ1Eqi7I3EdN1eRB7vfif8LMHu','admin@email.com.br',true);

/* INSERE CODIGO DO SISTEMA, CASO NÃO EXISTA */
INSERT INTO TBG_sistema (SIST_descricao,SIST_codigo) SELECT * FROM (SELECT 'Sistema Modelo' nome,'MODEL' codigo) AS tmp WHERE NOT EXISTS (SELECT SIST_codigo FROM TBG_sistema WHERE SIST_codigo = 'MODEL');

/* GUARDA O ID DO SISTEMA NA VARIÁVEL */
SELECT @SISTEMA := SIST_id FROM TBG_sistema WHERE SIST_codigo = 'MODEL' LIMIT 1;

/* CRIA CLASSE 'ADM' NO SISTEMA, CASO AINDA NÃO EXISTA */
INSERT INTO TBG_classe(CLA_descricao,CLA_codigo,CLA_admin,CLA_FK_SIST_id) SELECT * FROM (SELECT 'Administrador','ADM',1,@SISTEMA) AS TMP WHERE NOT EXISTS(SELECT 1 FROM TBG_classe WHERE CLA_admin = 1 AND CLA_FK_SIST_id = @SISTEMA);

/* RELACIONA O USUÁRIO PADRÃO COM A CLASSE 'ADM' DO SISTEMA */
INSERT INTO TBG_permissao(PERM_FK_CLA_id,PERM_FK_USUA_id) VALUES((SELECT CLA_ID FROM TBG_classe WHERE CLA_codigo = 'ADM' AND CLA_FK_SIST_id = @SISTEMA),(SELECT USUA_ID FROM TBG_usuario WHERE USUA_cpf = '12345678910'));


/*################################ ROTAS ################################*/

/**** Menu Configurações ****/
INSERT INTO TBG_rotas(ROTA_nome,ROTA_rota,ROTA_menu,ROTA_icone,ROTA_FK_SIST_id,ROTA_FK_ROTA_id)
VALUES
/* Menu 'Admin' */
('Admin','config',1,'fas fa-cog fa-lg',@SISTEMA,NULL)

/* Sistema */
,('Sistema','adm.sistema',1,'fas fa-laptop',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'config' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('salva sistema','adm.sistema.salvar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.sistema' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('deleta sistema','adm.sistema.delete',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.sistema' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))

/* Classe*/
,('Classes','adm.classe',1,'fas fa-user-tag',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'config' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('salva classe','adm.classe.salvar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.classe' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('deleta classe','adm.classe.delete',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.classe' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))

/* Rotas */
,('Rotas','adm.rota',1,'fas fa-route',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'config' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('salva rotas','adm.rota.salvar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.rota' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('delete rotas','adm.rota.delete',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.rota' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))

/* Classes x Rotas */
,('Classe x Rotas','adm.classe_rotas',1,'fas fa-code-branch',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'config' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('associa classes x rotas','adm.classe_rotas.ajax-associa-rotas',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.rota' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))

/* Usuários */
,('Usuários','adm.usuario',1,'fas fa-user-friends fa-lg',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'config' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('salva usuário','adm.usuario.salvar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.usuario' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('delete usuário','adm.usuario.delete',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.usuario' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1));



/**** menus estados e cidades opcionais ****/
INSERT INTO TBG_rotas(ROTA_nome,ROTA_rota,ROTA_menu,ROTA_icone,ROTA_FK_SIST_id,ROTA_FK_ROTA_id)
VALUES
/* Estados */
('Estado','adm.uf',1,'fas fa-caret-right',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'config' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('salva UF','adm.uf.salvar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.uf' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))

 /* Cidades */
,('Cidades','adm.cidade',1,'fas fa-caret-right',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'config' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('salva cidade','adm.cidade.salvar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'adm.cidade' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1));



/**** Menus do Sistema ****/
/*--------------- adicione os menus específicos do sistema a partir deste ponto ---------------*/

/*--------------- <menus específicos do sistema> ---------------*/

/*
INSERT INTO TBG_rotas(ROTA_nome,ROTA_rota,ROTA_menu,ROTA_icone,ROTA_FK_SIST_id,ROTA_FK_ROTA_id)
VALUES
('Cadastro','cadastro',1,'fas fa-pencil-alt fa-lg',@SISTEMA,NULL)
,('Marca','cadastro.marca',1,'fas fa-caret-right',@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'cadastro' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('salva marca','cadastro.marca.salvar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'cadastro.marca' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1))
,('deleta marca','cadastro.marca.deletar',0,NULL,@SISTEMA,(SELECT r.ROTA_id FROM TBG_rotas r WHERE r.ROTA_rota = 'cadastro.marca' AND r.ROTA_FK_SIST_id = @SISTEMA LIMIT 1));
*/

/*--------------- </menus específicos do sistema> ---------------*/


/*RELACIONA TODAS AS ROTAS DO SISTEMA COM A CLASSE 'ADM'*/
INSERT INTO TBG_classe_rota(CLARO_FK_CLA_id,CLARO_FK_ROTA_id)
SELECT
    (SELECT CLA_ID FROM TBG_classe WHERE CLA_codigo = 'ADM' AND CLA_FK_SIST_id = @SISTEMA LIMIT 1),
    ROTA_id
FROM TBG_rotas
WHERE ROTA_FK_SIST_id = @SISTEMA;
