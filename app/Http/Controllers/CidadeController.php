<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Cidade;

class CidadeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexCidade(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}

        $cidades = Cidade::orderby(Cidade::$nome)->get();
        $estados = UfController::GetComboEstado();
        
        return view('adm.cidade',['cidades'=>$cidades,'estados'=>$estados]);
    }

    public function salvarCidade(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.cidade');}

        try
        {
            if(!empty(Cidade::where(Cidade::$fk_estado,$request->uf)->where(Cidade::$nome,$request->nome)->first()))
            {
                return Redirect::route('adm.cidade')->withErrors('Já existe uma Cidade com este nome no estado selecionado.');
            }

            $cidade = Cidade::findOrNew($request->id);
            $cidade->setNome($request->nome);
            $cidade->setFkEstado($request->uf);
            
            $uf->save();

            return Redirect::route('adm.cidade');
        }
        catch(\Exception $ex)
        {
            return Redirect::route('adm.cidade')->withErrors('erro ao salvar o Estado: '.$ex->getMessage());
        }
    }
}
