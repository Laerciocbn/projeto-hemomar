<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Especialidade;
use Hash;

class EspecialidadeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $rota='cadastro.especialidade';
    public function indexEspecialidade()
    {
        try
        {
            $especialidades = Especialidade::all();

            return View('cadastro.especialidade',['especialidades'=>$especialidades]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route(home)->withErrors('erro ao carregar a Especialidade: '.$ex->getMessage());
        }
    }

    public function salvarEspecialidade(Request $request)
    {
        try
        {
            $especialidade = Especialidade::findOrNew($request->id);
            $especialidade->setDescricao($request->descricao);
            $especialidade->setAtuacao($request->atuacao);
            $especialidade->setAtivo(!empty(($request->ativo)));

            $especialidade->save();

            return Redirect::route('cadastro.especialidade');
        }
        catch(\Exception $ex)
        {
            return Redirect::route($this->rota)->withErrors('erro ao salvar a especialidade: '.$ex->getMessage());
        }
    }

    public function deleteEspecialidade(Request $request)
    {
        try
        {
            $especialidade = Especialidade::find($request->id);
            $especialidade->delete();

            return Redirect::route('cadastro.especialidade');
        }
        catch(\Exception $ex)
        {
            return Redirect::route($this->rota)->withErrors('erro ao excluir a especialidade: '.$ex->getMessage());
        }
    }

}
