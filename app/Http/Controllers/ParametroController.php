<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Parametro;

class ParametroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     //############ PARÂMETRO ############
     public function indexParametro(Request $resquest)
     {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}

        try
        {
            $parametros = Parametro::all();

            return view('adm.parametros',['parametros'=>$parametros]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route('home')->withErrors('erro ao carregar os tipos de veículos: '.$ex->getMessage());
        }
     }

    public function salvarParametro(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.parametro');}

        try
        {
            $p = Parametro::where(Parametro::$codigo,$request->codigo)->first();

            if((empty($request->id) && !empty($p)) || (!empty($request->id) && !empty($p) && $request->id != $p->getId()))
            {
                return Redirect::route('adm.parametro')->withErrors('Já existe um parametro com este código.');
            }

            $parametro = Parametro::findOrNew($request->id);
            $parametro->setCodigo($request->codigo);
            $parametro->setDescricao($request->descricao);
            $parametro->setValor($request->valor);

            $parametro->save();

            return Redirect::route('adm.parametro');
        }
        catch(\Exception $ex)
        {
            return Redirect::route('adm.parametro')->withErrors('erro ao salvar o parâmetro: '.$ex->getMessage());
        }
    }

    public function deleteParametro(Request $request)
    {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.parametro');}

        try
        {
            if(empty($request->id))
            {
                return Redirect::route('adm.parametro')->withErrors('Código do parâmetro vazio.');
            }

            $parametro = Parametro::find($request->id);
            $parametro->delete();

            return Redirect::route('adm.parametro');
        }
        catch(\Exception $ex)
        {
            return Redirect::route('adm.parametro')->withErrors('erro ao excluir o parâmetro: '.$ex->getMessage());
        }
    }
 
}
