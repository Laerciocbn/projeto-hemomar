<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Convenio;

class ConvenioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     //############ SISTEMA ############
     public function indexConvenio(Request $request)
     {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}

        try
        {
            $convenio = Convenio::all();
            return view('cadastro.convenio',['convenio'=>$convenio]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route('home')->withErrors('erro ao carregar o convênio: '.$ex->getMessage());
        }
     }


 
     public static function GetComboConvenio()
     {
         return Convenio::orderby(Convenio::$sigla)->get()
                         ->keyBy(Convenio::$id)->map(function ($item)
                         {
                             return $item->getSigla();
                         });
     }
    
 
}
