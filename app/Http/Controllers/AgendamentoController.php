<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Agendamento;

class AgendamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     //############ SISTEMA ############
     public function indexAgendamento(Request $request)
     {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}

        try
        {
            $agendamento = Agendamento::all();
            $convenio = ConvenioController::GetComboConvenio();
            $paciente = PacienteController::GetComboPaciente();
            $exame = ExameController::GetComboExame();

            return view('atendimemto.agendamento',
                        [
                            'agendamento'=>$agendamento,
                            'convenio'=>$convenio,
                            'paciente'=>$paciente,
                            'exame'=>$exame
                        ]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route('home')->withErrors('erro ao carregar os sistemas: '.$ex->getMessage());
        }
     }

     public function salvarAgendamento(Request $request)
     {
         //if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.sistema');}
 
         try
         {
             $sist = Sistema::where(Sistema::$codigo,$request->codigo)->first();
 
             if((empty($request->id) && !empty($sist)) || (!empty($request->id) && !empty($sist) && $request->id != $sist->getId()))
             {
                 return Redirect::route('adm.sistema')->withErrors('Já existe um sistema com este código.');
             }
 
             $sistema = Sistema::findOrNew($request->id);
             $sistema->setCodigo($request->codigo);
             $sistema->setDescricao($request->descricao);
 
             $sistema->save();
         }
         catch(\Exception $ex)
         {
             return Redirect::route('adm.sistema')->withErrors('erro ao salvar o sistema: '.$ex->getMessage());
         }
 
         return Redirect::route('adm.sistema');
     }
 
     public function deleteAgendamento(Request $request)
     {
         if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('adm.sistema');}
 
         try
         {
             if(empty($request->id))
             {
                 return Redirect::route('adm.sistema')->withErrors('Código do sistema vazio.');
             }
 
             $sistema = Sistema::find($request->id);
             $sistema->delete();
         }
         catch(\Exception $ex)
         {
             return Redirect::route('adm.sistema')->withErrors('erro ao excluir o sistema: '.$ex->getMessage());
         }
 
         return Redirect::route('adm.sistema');
     }
  
 
    
 
}
