<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Exames;

class ExamesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     //############ Exames ############
     public function indexExames(Request $request)
     {
        if(!RotaController::Acesso($request->route()->getName())){return RotaController::AcessoNegado('home');}

        try
        {
            $exames = Exames::all();
            return view('cadastro.exames',['exames'=>$exames]);
        }
        catch(\Exception $ex)
        {
            return Redirect::route('home')->withErrors('erro ao carregar o Exame: '.$ex->getMessage());
        }
     }


 
     public static function GetComboExames()
     {
         return Exames::orderby(Exames::$descricao)->get()
                         ->keyBy(Exames::$id)->map(function ($item)
                         {
                             return $item->getDescricao();
                         });
     }
    
 
}
