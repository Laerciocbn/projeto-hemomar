<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Paciente;
use Hash;

class PacienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexPaciente()
    {
        try
        {
            $pacientes = Paciente::all();

            return View('cadastro.paciente',['pacientes'=>$pacientes]);
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao carregar os Pacientes: '.$ex->getMessage());
        }
    }

    public function salvarPaciente(Request $request)
    {
        try
        {
            $cpf = str_replace('-','',str_replace('.','',$request->cpf));

            $paciente = Paciente::where(Paciente::$cpf,$cpf)->first();

            if((empty($request->id) && !empty($paciente)) || (!empty($request->id) && !empty($paciente) && $paciente->getId() != $request->id))
            {
                return Redirect::back()->withErrors('Já existe um médico cadastrado com o CPF informado.');
            }

            $paciente = Paciente::findOrNew($request->id);
            $paciente->setNome($request->nome);
            $paciente->setCpf($request->cpf);
            $paciente->setEmail($request->email);
            $paciente->setTelefone($request->telefone);
            $paciente->setAtivo(!empty(($request->ativo)));
            //$paciente->setAtivo(!empty(($request->ativo) ? '1' : '0'));

            $paciente->save();

            return Redirect::route('cadastro.paciente');
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao salvar o paciente: '.$ex->getMessage());
        }
    }

    public function deletePaciente(Request $request)
    {
        try
        {
            $paciente = Paciente::find($request->id);
            $paciente->delete();

            return Redirect::route('cadastro.paciente');
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao excluir o paciente: '.$ex->getMessage());
        }
    }

    public static function GetComboPaciente()
    {
        return Paciente::orderby(Paciente::$nome)->get()
                        ->keyBy(Paciente::$id)->map(function ($item)
                        {
                            return $item->getNome();
                        });
    }
   

}
