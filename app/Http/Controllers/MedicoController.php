<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\Medico;
use Hash;

class MedicoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexMedico()
    {
        try
        {
            $medicos = Medico::all();

            return View('cadastro.medico',['medicos'=>$medicos]);
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao carregar os Médicos: '.$ex->getMessage());
        }
    }

    public function salvarMedico(Request $request)
    {
        try
        {
            $cpf = str_replace('-','',str_replace('.','',$request->cpf));

            $medico = Medico::where(Medico::$cpf,$cpf)->first();

            if((empty($request->id) && !empty($medico)) || (!empty($request->id) && !empty($medico) && $medico->getId() != $request->id))
            {
                return Redirect::back()->withErrors('Já existe um médico cadastrado com o CPF informado.');
            }

            $medico = Medico::findOrNew($request->id);
            $medico->setNome($request->nome);
            $medico->setCpf($request->cpf);
            $medico->setCrm($request->crm);
            $medico->setEmail($request->email);
            $medico->setTelefone($request->telefone);
            $medico->setAtivo(!empty(($request->ativo)));
            //$medico->setAtivo(!empty(($request->ativo) ? '1' : '0'));

            $medico->save();

            return Redirect::route('cadastro.medico');
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao salvar o médico: '.$ex->getMessage());
        }
    }

    public function deleteMedico(Request $request)
    {
        try
        {
            $medico = Medico::find($request->id);
            $medico->delete();

            return Redirect::route('cadastro.medico');
        }
        catch(\Exception $ex)
        {
            return Redirect::back()->withErrors('erro ao excluir o médico: '.$ex->getMessage());
        }
    }

}
