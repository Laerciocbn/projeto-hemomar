<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsavel extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_responsavel';
	protected $primaryKey = 'RESP_id';
	public $timestamps = false;

	//campos
	public static $id = 'RESP_id';
	public static $fk_paciente = 'RESP_PACI_id';
	public static $fk_acompanhante = 'RESP_ACOM_id';

	//Relacionamentos
	public function Paciente(){return $this->belongsTo('App\Paciente',Responsavel::$fk_paciente);}
	public function Acompanhante(){return $this->belongsTo('App\Acompanhante',Responsavel::$fk_acompanhante);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Responsavel::$id];}
	public function getPaci(){return $this->attributes[Responsavel::$fk_paciente];}
	public function getAcom(){return $this->attributes[Responsavel::$fk_acompanhante];}

	//Set's
	public function setId($valor){$this->attributes[Responsavel::$id] = $valor;}
	public function setPaci($valor){$this->attributes[Responsavel::$fk_paciente] = $valor;}
	public function setAcom($valor){$this->attributes[Responsavel::$fk_acompanhante] = $valor;}

}