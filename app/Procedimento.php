<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedimento extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_procedimento';
	protected $primaryKey = 'PROC_id';
	public $timestamps = false;

	//campos
	public static $id = 'PROC_id';
	public static $descricao = 'PROC_descricao';

	//Relacionamentos
	public function Agendas(){return $this->hasMany('App\Agenda',Agenda::$fk_procedimento);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Procedimento::$id];}
	public function getDescricao(){return $this->attributes[Procedimento::$descricao];}

	//Set's
	public function setId($valor){$this->attributes[Procedimento::$id] = $valor;}
	public function setDescricao($valor){$this->attributes[Procedimento::$descricao] = $valor;}

}