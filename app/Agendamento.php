<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Agendamento extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_agendamento';
	protected $primaryKey = 'AGEN_id';
	protected $dates = ['AGEN_inicio','AGEN_fim','AGEN_dataCad'];
	public $timestamps = false;

	//campos
	public static $id = 'AGEN_id';
	public static $fk_convenio = 'AGEN_FK_CONV_id';
	public static $fk_usuario = 'AGEN_FK_USUA_id';
	public static $fk_paciente = 'AGEN_FK_PACI_id';
	public static $fk_exames = 'AGEN_FK_EXAM_id';
	public static $fk_procedimento = 'AGEN_FK_PROC_id';
	public static $fk_agendamedico = 'AGEN_FK_AGEN_MEDI_id';
	public static $titulo = 'AGEN_titulo';
	public static $inicio = 'AGEN_inicio';
	public static $fim = 'AGEN_fim';
	public static $obs = 'AGEN_obs';
	public static $status = 'AGEN_status';
	public static $datacad = 'AGEN_dataCad';

	//Relacionamentos
	public function Convenio(){return $this->belongsTo('App\Convenio',Agendamento::$fk_convenio);}
	public function Usuario(){return $this->belongsTo('App\Usuario',Agendamento::$fk_usuario);}
	public function Paciente(){return $this->belongsTo('App\Paciente',Agendamento::$fk_paciente);}
	public function Exames(){return $this->belongsTo('App\Exames',Agendamento::$fk_exames);}
	public function Procedimento(){return $this->belongsTo('App\Procedimento',Agendamento::$fk_procedimento);}
	public function AgendaMedico(){return $this->belongsTo('App\AgendaMedico',Agendamento::$fk_agendamedico);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Agendamento::$id];}
	public function getFkConv(){return $this->attributes[Agendamento::$fk_convenio];}
	public function getFkUsua(){return $this->attributes[Agendamento::$fk_usuario];}
	public function getFkPaci(){return $this->attributes[Agendamento::$fk_paciente];}
	public function getFkExam(){return $this->attributes[Agendamento::$fk_exames];}
	public function getFkProc(){return $this->attributes[Agendamento::$fk_procedimento];}
	public function getFkAgenMedi(){return $this->attributes[Agendamento::$fk_agendamedico];}
	public function getTitulo(){return $this->attributes[Agendamento::$titulo];}
	public function getInicio(){return (new Carbon($this->attributes[Agendamento::$inicio]))->format('d/m/Y H:i:s');}
	public function getFim(){return (new Carbon($this->attributes[Agendamento::$fim]))->format('d/m/Y H:i:s');}
	public function getObs(){return $this->attributes[Agendamento::$obs];}
	public function getStatus(){return $this->attributes[Agendamento::$status];}
	public function getDatacad(){return (new Carbon($this->attributes[Agendamento::$datacad]))->format('d/m/Y H:i:s');}

	//Set's
	public function setId($valor){$this->attributes[Agendamento::$id] = $valor;}
	public function setFkConv($valor){$this->attributes[Agendamento::$fk_convenio] = $valor;}
	public function setFkUsua($valor){$this->attributes[Agendamento::$fk_usuario] = $valor;}
	public function setFkPaci($valor){$this->attributes[Agendamento::$fk_paciente] = $valor;}
	public function setFkExam($valor){$this->attributes[Agendamento::$fk_exames] = $valor;}
	public function setFkProc($valor){$this->attributes[Agendamento::$fk_procedimento] = $valor;}
	public function setFkAgenMedi($valor){$this->attributes[Agendamento::$fk_agendamedico] = $valor;}
	public function setTitulo($valor){$this->attributes[Agendamento::$titulo] = $valor;}
	public function setInicio($valor){$this->attributes[Agendamento::$inicio] = Carbon::createFromFormat('d/m/Y H:i:s',$valor);}
	public function setFim($valor){$this->attributes[Agendamento::$fim] = Carbon::createFromFormat('d/m/Y H:i:s',$valor);}
	public function setObs($valor){$this->attributes[Agendamento::$obs] = $valor;}
	public function setStatus($valor){$this->attributes[Agendamento::$status] = $valor;}
	public function setDatacad($valor){$this->attributes[Agendamento::$datacad] = Carbon::createFromFormat('d/m/Y H:i:s',$valor);}

}