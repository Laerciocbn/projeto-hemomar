<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
   protected $connection = 'mysql';
    protected $table = 'TBG_cidade';
    protected $primaryKey = 'CIDA_id';
    public $timestamps = false;

    //campos
    public static $id = 'CIDA_id';
    public static $nome = 'CIDA_descricao';
    public static $fk_estado = 'CIDA_FK_UF_id';

    //relacionamentos
    public function Estado()
    {
        return $this->belongsTo('App\Estado',Cidade::$fk_estado);
    }

    //atributos
    public function getId()
    {
        return $this->attributes[Cidade::$id];
    }

    public function getNome()
    {
        return $this->attributes[Cidade::$nome];
    }

    public function getFkEstado()
    {
        return $this->attributes[Cidade::$fk_estado];
    }

    public function setNome($valor)
    {
        $this->attributes[Cidade::$nome] = $valor;
    }

    public function setFkEstado($valor)
    {
        $this->attributes[Cidade::$fk_estado] = $valor;
    }
}
