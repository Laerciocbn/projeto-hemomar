<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{
    protected $connection = 'mysql';
    protected $table = 'TBG_classe';
    protected $primaryKey = 'CLA_id';
    public $timestamps = false;

    //campos
    public static $id = 'CLA_id';
    public static $codigo = 'CLA_codigo';
    public static $descricao = 'CLA_descricao';
    public static $admin = 'CLA_admin';
    public static $fk_sistema = 'CLA_FK_SIST_id';

    //Relacionamento
    public function Sistema()
    {
        return $this->belongsTo('App\Sistema',Classe::$fk_sistema);
    }
    public function Rotas()
    {
        return $this->belongsToMany('App\Rota',Classe_Rota::$tabela,Classe_Rota::$fk_classe,Classe_Rota::$fk_rota);
    }

    public function RotasMenu()
    {
        return $this->belongsToMany('App\Rota',Classe_Rota::$tabela,Classe_Rota::$fk_classe,Classe_Rota::$fk_rota)
                    ->whereNull(Rota::$fk_rota_pai)
                    ->where(Rota::$menu,true)
                    ->with(['Rotas'=>function($rota)
                    {
                        $rota->whereIn(Rota::$id,$this->Rotas->pluck(Rota::$id)->toArray())->orderby(Rota::$index);
                    }])
                    ->orderby(Rota::$index);
    }

    //atributos
    public function getId()
    {
        return $this->attributes[Classe::$id];
    }
    public function getCodigo()
    {
        return $this->attributes[Classe::$codigo];
    }
    public function getDescricao()
    {
        return $this->attributes[Classe::$descricao];
    }
    public function isAdmin()
    {
        return $this->attributes[Classe::$admin];
    }
    public function getFkSistema()
    {
        return $this->attributes[Classe::$fk_sistema];
    }


    public function setCodigo($valor)
    {
        $this->attributes[Classe::$codigo] = $valor;
    }
    public function setDescricao($valor)
    {
        $this->attributes[Classe::$descricao] = $valor;
    }
    public function setAdmin($valor)
    {
        $this->attributes[Classe::$admin] = $valor;
    }
    public function setFkSistema($valor)
    {
        $this->attributes[Classe::$fk_sistema] = $valor;
    }
}
