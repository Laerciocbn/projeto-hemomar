<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_convenio';
	protected $primaryKey = 'CONV_id';
	public $timestamps = false;

	//campos
	public static $id = 'CONV_id';
	public static $descricao = 'CONV_descricao';
	public static $sigla = 'CONV_sigla';

	//Relacionamentos
	public function Agendas(){return $this->hasMany('App\Agenda',Agenda::$fk_convenio);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Convenio::$id];}
	public function getDescricao(){return $this->attributes[Convenio::$descricao];}
	public function getSigla(){return $this->attributes[Convenio::$sigla];}

	//Set's
	public function setId($valor){$this->attributes[Convenio::$id] = $valor;}
	public function setDescricao($valor){$this->attributes[Convenio::$descricao] = $valor;}
	public function setSigla($valor){$this->attributes[Convenio::$sigla] = $valor;}

}