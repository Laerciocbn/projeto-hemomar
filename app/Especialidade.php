<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidade extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_especialidade';
	protected $primaryKey = 'ESPE_id';
	public $timestamps = false;

	//campos
	public static $id = 'ESPE_id';
	public static $descricao = 'ESPE_descricao';
	public static $atuacao = 'ESPE_atuacao';
	public static $ativo = 'ESPE_ativo';

	//Relacionamentos
	public function Medicos(){return $this->hasMany('App\Medico',Medico::$fk_especialidade);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Especialidade::$id];}
	public function getDescricao(){return $this->attributes[Especialidade::$descricao];}
	public function getAtuacao(){return $this->attributes[Especialidade::$atuacao];}
	public function getAtivo(){return $this->attributes[Especialidade::$ativo];}

	//Set's
	public function setId($valor){$this->attributes[Especialidade::$id] = $valor;}
	public function setDescricao($valor){$this->attributes[Especialidade::$descricao] = $valor;}
	public function setAtuacao($valor){$this->attributes[Especialidade::$atuacao] = $valor;}
	public function setAtivo($valor){$this->attributes[Especialidade::$ativo] = $valor;}

}