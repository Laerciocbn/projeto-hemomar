<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acompanhante extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_acompanhante';
	protected $primaryKey = 'ACOM_id';
	public $timestamps = false;

	//campos
	public static $id = 'ACOM_id';
	public static $nome = 'ACOM_nome';
	public static $contato = 'ACOM_contato';

	//Relacionamentos
	public function Responsaveis(){return $this->hasMany('App\Responsavel',Responsavel::$fk_acompanhante);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Acompanhante::$id];}
	public function getNome(){return $this->attributes[Acompanhante::$nome];}
	public function getContato(){return $this->attributes[Acompanhante::$contato];}

	//Set's
	public function setId($valor){$this->attributes[Acompanhante::$id] = $valor;}
	public function setNome($valor){$this->attributes[Acompanhante::$nome] = $valor;}
	public function setContato($valor){$this->attributes[Acompanhante::$contato] = $valor;}

}