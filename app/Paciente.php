<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Paciente extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_paciente';
	protected $primaryKey = 'PACI_id';
	protected $dates = ['PACI_dataNasc','PACI_dataCad'];
	public $timestamps = false;

	//campos
	public static $id = 'PACI_id';
	public static $nome = 'PACI_nome';
	public static $datanasc = 'PACI_dataNasc';
	public static $rg = 'PACI_rg';
	public static $cpf = 'PACI_cpf';
	public static $naturalidade = 'PACI_naturalidade';
	public static $estadocivil = 'PACI_estadoCivil';
	public static $grauinsntrucao = 'PACI_grauInsntrucao';
	public static $gruposangue = 'PACI_grupoSangue';
	public static $sexo = 'PACI_sexo';
	public static $pai = 'PACI_pai';
	public static $mae = 'PACI_mae';
	public static $email = 'PACI_email';
	public static $endereco = 'PACI_endereco';
	public static $numero = 'PACI_numero';
	public static $bairro = 'PACI_bairro';
	public static $fk_cidade = 'PACI_FK_CIDA_id';
	public static $cep = 'PACI_cep';
	public static $telefone = 'PACI_telefone';
	public static $datacad = 'PACI_dataCad';
	public static $ativo = 'PACI_ativo';

	//Relacionamentos
	public function Agendamentos(){return $this->hasMany('App\Agendamento',Agendamento::$fk_paciente);}
	public function Responsaveis(){return $this->hasMany('App\Responsavel',Responsavel::$fk_paciente);}
	public function Cidade(){return $this->belongsTo('App\Cidade',Paciente::$fk_cidade);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Paciente::$id];}
	public function getNome(){return $this->attributes[Paciente::$nome];}
	public function getDatanasc(){return (new Carbon($this->attributes[Paciente::$datanasc]))->format('d/m/Y');}
	public function getRg(){return $this->attributes[Paciente::$rg];}
	public function getCpf(){return $this->attributes[Paciente::$cpf];}
	public function getNaturalidade(){return $this->attributes[Paciente::$naturalidade];}
	public function getEstadocivil(){return $this->attributes[Paciente::$estadocivil];}
	public function getGrauinsntrucao(){return $this->attributes[Paciente::$grauinsntrucao];}
	public function getGruposangue(){return $this->attributes[Paciente::$gruposangue];}
	public function getSexo(){return $this->attributes[Paciente::$sexo];}
	public function getPai(){return $this->attributes[Paciente::$pai];}
	public function getMae(){return $this->attributes[Paciente::$mae];}
	public function getEmail(){return $this->attributes[Paciente::$email];}
	public function getEndereco(){return $this->attributes[Paciente::$endereco];}
	public function getNumero(){return $this->attributes[Paciente::$numero];}
	public function getBairro(){return $this->attributes[Paciente::$bairro];}
	public function getFkCida(){return $this->attributes[Paciente::$fk_cidade];}
	public function getCep(){return $this->attributes[Paciente::$cep];}
	public function getTelefone(){return $this->attributes[Paciente::$telefone];}
	public function getDatacad(){return (new Carbon($this->attributes[Paciente::$datacad]))->format('d/m/Y H:i:s');}
	public function getAtivo(){return $this->attributes[Paciente::$ativo];}

	//Set's
	public function setId($valor){$this->attributes[Paciente::$id] = $valor;}
	public function setNome($valor){$this->attributes[Paciente::$nome] = $valor;}
	public function setDatanasc($valor){$this->attributes[Paciente::$datanasc] = Carbon::createFromFormat('d/m/Y',$valor);}
	public function setRg($valor){$this->attributes[Paciente::$rg] = $valor;}
	public function setCpf($valor){$this->attributes[Paciente::$cpf] = $valor;}
	public function setNaturalidade($valor){$this->attributes[Paciente::$naturalidade] = $valor;}
	public function setEstadocivil($valor){$this->attributes[Paciente::$estadocivil] = $valor;}
	public function setGrauinsntrucao($valor){$this->attributes[Paciente::$grauinsntrucao] = $valor;}
	public function setGruposangue($valor){$this->attributes[Paciente::$gruposangue] = $valor;}
	public function setSexo($valor){$this->attributes[Paciente::$sexo] = $valor;}
	public function setPai($valor){$this->attributes[Paciente::$pai] = $valor;}
	public function setMae($valor){$this->attributes[Paciente::$mae] = $valor;}
	public function setEmail($valor){$this->attributes[Paciente::$email] = $valor;}
	public function setEndereco($valor){$this->attributes[Paciente::$endereco] = $valor;}
	public function setNumero($valor){$this->attributes[Paciente::$numero] = $valor;}
	public function setBairro($valor){$this->attributes[Paciente::$bairro] = $valor;}
	public function setFkCida($valor){$this->attributes[Paciente::$fk_cidade] = $valor;}
	public function setCep($valor){$this->attributes[Paciente::$cep] = $valor;}
	public function setTelefone($valor){$this->attributes[Paciente::$telefone] = $valor;}
	public function setDatacad($valor){$this->attributes[Paciente::$datacad] = Carbon::createFromFormat('d/m/Y H:i:s',$valor);}
	public function setAtivo($valor){$this->attributes[Paciente::$ativo] = $valor;}

}