<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exames extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_exames';
	protected $primaryKey = 'EXAM_id';
	public $timestamps = false;

	//campos
	public static $id = 'EXAM_id';
	public static $descricao = 'EXAM_descricao';
	public static $codigo = 'EXAM_codigo';

	//Relacionamentos
	public function Agendas(){return $this->hasMany('App\Agenda',Agenda::$fk_exames);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Exames::$id];}
	public function getDescricao(){return $this->attributes[Exames::$descricao];}
	public function getCodigo(){return $this->attributes[Exames::$codigo];}

	//Set's
	public function setId($valor){$this->attributes[Exames::$id] = $valor;}
	public function setDescricao($valor){$this->attributes[Exames::$descricao] = $valor;}
	public function setCodigo($valor){$this->attributes[Exames::$codigo] = $valor;}

}