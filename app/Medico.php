<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Medico extends Model
{
	protected $connection = 'mysql';
	protected $table = 'HEMO_medico';
	protected $primaryKey = 'MEDI_id';
	protected $dates = ['MEDI_dataNasc','MEDI_dataCad'];
	public $timestamps = false;

	//campos
	public static $id = 'MEDI_id';
	public static $nome = 'MEDI_nome';
	public static $datanasc = 'MEDI_dataNasc';
	public static $sexo = 'MEDI_sexo';
	public static $cpf = 'MEDI_cpf';
	public static $crm = 'MEDI_crm';
	public static $cbo = 'MEDI_cbo';
	public static $telefone = 'MEDI_telefone';
	public static $email = 'MEDI_email';
	public static $endereco = 'MEDI_endereco';
	public static $bairro = 'MEDI_bairro';
	public static $fk_cidade = 'MEDI_FK_CIDA_id';
	public static $fk_especialidades = 'MEDI_FK_ESPE_id';
	public static $datacad = 'MEDI_dataCad';
	public static $ativo = 'MEDI_ativo';

	//Relacionamentos
	public function AgendaMedicos(){return $this->hasMany('App\AgendaMedico',AgendaMedico::$fk_medico);}
	public function Cidade(){return $this->belongsTo('App\Cidade',Medico::$fk_cidade);}
	public function Especialidades(){return $this->belongsTo('App\Especialidades',Medico::$fk_especialidades);}

	//Get's
	public function getTabela(){return $this->$table;}
	public function getId(){return $this->attributes[Medico::$id];}
	public function getNome(){return $this->attributes[Medico::$nome];}
	public function getDatanasc(){return (new Carbon($this->attributes[Medico::$datanasc]))->format('d/m/Y');}
	public function getSexo(){return $this->attributes[Medico::$sexo];}
	public function getCpf(){return $this->attributes[Medico::$cpf];}
	public function getCrm(){return $this->attributes[Medico::$crm];}
	public function getCbo(){return $this->attributes[Medico::$cbo];}
	public function getTelefone(){return $this->attributes[Medico::$telefone];}
	public function getEmail(){return $this->attributes[Medico::$email];}
	public function getEndereco(){return $this->attributes[Medico::$endereco];}
	public function getBairro(){return $this->attributes[Medico::$bairro];}
	public function getFkCida(){return $this->attributes[Medico::$fk_cidade];}
	public function getFkEspe(){return $this->attributes[Medico::$fk_especialidades];}
	public function getDatacad(){return (new Carbon($this->attributes[Medico::$datacad]))->format('d/m/Y H:i:s');}
	public function getAtivo(){return $this->attributes[Medico::$ativo];}

	//Set's
	public function setId($valor){$this->attributes[Medico::$id] = $valor;}
	public function setNome($valor){$this->attributes[Medico::$nome] = $valor;}
	public function setDatanasc($valor){$this->attributes[Medico::$datanasc] = Carbon::createFromFormat('d/m/Y',$valor);}
	public function setSexo($valor){$this->attributes[Medico::$sexo] = $valor;}
	public function setCpf($valor){$this->attributes[Medico::$cpf] = $valor;}
	public function setCrm($valor){$this->attributes[Medico::$crm] = $valor;}
	public function setCbo($valor){$this->attributes[Medico::$cbo] = $valor;}
	public function setTelefone($valor){$this->attributes[Medico::$telefone] = $valor;}
	public function setEmail($valor){$this->attributes[Medico::$email] = $valor;}
	public function setEndereco($valor){$this->attributes[Medico::$endereco] = $valor;}
	public function setBairro($valor){$this->attributes[Medico::$bairro] = $valor;}
	public function setFkCida($valor){$this->attributes[Medico::$fk_cidade] = $valor;}
	public function setFkEspe($valor){$this->attributes[Medico::$fk_especialidades] = $valor;}
	public function setDatacad($valor){$this->attributes[Medico::$datacad] = Carbon::createFromFormat('d/m/Y H:i:s',$valor);}
	public function setAtivo($valor){$this->attributes[Medico::$ativo] = $valor;}

}